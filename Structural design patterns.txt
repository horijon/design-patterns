Structural Pattern provides us ways to club classes and object together to form large structures.

1) Proxy
	scenarios for this pattern:-
	a) The object being represented is external to the system.
	b) Objects need to be created on demand.
	c) Access control for the original object is required.
	d) Added functionality is required when an object is accessed.
	https://dzone.com/articles/design-patterns-proxy
	acts as a "Placeholder" for an object to control references to it
	
2) Adapter
	acts as an adapter(translator or middleman) between two incompatible interfaces
	if the implementation we have does not support the expected; we create an adapter and interface(also implementation)
		so that we achieve the required by calling the same unexpected implementation
	eg; if an interface only supports mp3 player, and we now try to play mp4 or vlc from the mp3 player, then for this to work,
		we need to create an adapter which is called by the mp3 player, the adapter then calls to the interface needed(implementation
		i.e either mp4 or vlc according to clients expectation; this implementation should also be created)
		youtube videos are best for this design pattern
		
3) Bridge
	allows the abstraction and implementation to be developed independently(decouples an abstraction from implementation)
	creates two different hierarchies: one for abstraction and another for implementation
	uses encapsulation and generally composition/aggregation(rather than inheritance) to achieve independency
	eg; and best explanation:-
		https://stackoverflow.com/questions/35079629/does-the-bridge-pattern-decouples-an-abstraction-from-implementation
		https://www.youtube.com/watch?v=epoPMsVG0XE&list=PLmCsXDGbJHdjoZ98R74y_nXfT60pOT2h4&index=11

4) Composite
	should be applied only when the group of objects should behave as the single object
	can be used to create a tree like structure
	When we need to create a structure in a way that the objects in the structure has to be treated the same way
	usually uses ArrayList
	http://www.journaldev.com/1535/composite-design-pattern-in-java

5) Flyweight
	used when large number of similar objects need to be created
	to reduce memory usage we share objects that are similar in some way rather than creating new ones
	usually uses HashMap to store objects
	https://www.tutorialspoint.com/design_pattern/flyweight_pattern.htm
	http://www.journaldev.com/1562/flyweight-design-pattern-java
	prototype pattern(creational) is used to create new instances, whereas flyweight(structural) is used to allow sharing of instances
		that have part of their internal state in common where the other part of state can vary
	two states: Instrinsic(constants; stored in the memory) and Extrinsic(not constants;
		need to be calculated on the fly and therefore not stored in the memory)
		eg; shape of angry bird is instrinsic(constants), color of angry bird is extrinsic(not constants)
	composite design pattern applies an action to a similar group of objects(like applied to the whole tree structure)
		whereas flyweight used to reduce object creation of similar type

6) Facade
	used to simplify the complex architecture and provide the client simple interface
	used when a number of operations of different classes need to be done in builk; eg a) open tv, check for satellite/cable functioning, switch to ESPN without facade => converted to(facade) just "watch ESPN"
	hides the complexities of the system and provides an interface to the client from where the client can access the system
	see other than first answer
		https://stackoverflow.com/questions/5242429/what-is-facade-design-pattern
	http://www.journaldev.com/1557/facade-design-pattern-in-java

7) Decorator
	used to modify the functionality of an object at runtime. At the same time other instances of the same class will not be affected by this,
		so individual object gets the modified behavior. It is one of the structural design pattern
		(such as Adapter Pattern, Bridge Pattern, Composite Pattern) and uses abstract classes or interface with composition to implement
	We use inheritance or composition to extend the behavior of an object but this is done at compile time
		and its applicable to all the instances of the class. We can’t add any new functionality or
		remove any existing behavior at runtime – this is when Decorator pattern comes into picture
	http://www.journaldev.com/1540/decorator-design-pattern-in-java-example
	https://www.tutorialspoint.com/design_pattern/decorator_pattern.htm

8) Filter
	